package com.example.demo.Topic;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class TopicController {

	@Autowired
	private TopicService topicservice;
	
	@RequestMapping("/Topics")
	 public List<Topic> getObjectst() {
	      return topicservice.getAllTopics();
	   }
	
	@RequestMapping("/Topics/{id}")
	public Topic getTopic(@PathVariable String id) {
	//	Topic t=new Topic("sofiene","gharbi","test");
		// topicservice.addTopic(t);
		return topicservice.getTopic(id);
									
	}

	@RequestMapping(method=RequestMethod.POST,value="/Topics")
	public void addTopic(@RequestBody Topic t) {
		
				topicservice.addTopic(t);
	}
		
}
