package com.example.demo.Topic;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class GreetingController {

  @GetMapping("/greeting")
  public String greetingForm(Model model) {
    model.addAttribute("greeting", new Greeting());
    String responsee="hahah";
    model.addAttribute("responsee",responsee);
    

    return "greeting";
  }

  @PostMapping("/greeting")
  public String greetingSubmit(@ModelAttribute Greeting greeting,Model model) {
	  
	  if(greeting.getId()==1) {
		  	String responsee="ghalet";
		    model.addAttribute("responsee",responsee);
		  responsee="Noooopeeeee";
		  return "greeting";
		  }
    return "result";
  }

}